﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication1.Models.AppSettings
{
    public class AppSettings
    {
        public string SecretKey { get; set; }
        public string EncryptKey { get; set; }
    }
}
