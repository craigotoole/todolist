﻿using Microsoft.EntityFrameworkCore;
using ToDoList.Models.API;

namespace ToDoList.Models.DatabaseConnection
{
    public class ToDoListDbContext : DbContext
    {

        public ToDoListDbContext(DbContextOptions<ToDoListDbContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<ToDo> ToDoList { get; set; }
    }
}
