﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.Models.API
{
    public class Login
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
