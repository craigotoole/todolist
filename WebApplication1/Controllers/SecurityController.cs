﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ToDoList.Models.API;
using ToDoList.Models.DatabaseConnection;
using WebApplication1.Models.API;
using WebApplication1.Models.AppSettings;
using WebApplication1.Models.EncryptDecrypt;

namespace ToDoList.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("CorsPolicy")]
    public class SecurityController : Controller
    {
        private readonly ToDoListDbContext _context;
        private readonly AppSettings _settings;

        public SecurityController(ToDoListDbContext context, IOptions<AppSettings> settings)
        {
            _context = context;
            _settings = settings.Value;
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] Login login)
        {
            var users = _context.Users.Where(user => user.Username == login.Username && EncryptDecrypt.Decrypt(user.Password, _settings.EncryptKey) == login.Password);

            if (users.Count() > 0)
            {
                var user = users.ToList().FirstOrDefault();

                var token = GenerateJWTToken(user);

                return Ok(token);
            }

            return BadRequest("Username or password was incorrect");
        }

        private Token GenerateJWTToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_settings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.Now.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            Token userToken = new Token();

            userToken.UserToken = tokenHandler.WriteToken(token);

            return userToken;
        }
    }
}