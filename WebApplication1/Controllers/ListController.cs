﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Models.API;
using ToDoList.Models.DatabaseConnection;

namespace WebApplication1.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("CorsPolicy")]
    public class ListController : Controller
    {
        private readonly ToDoListDbContext _context;

        public ListController(ToDoListDbContext context)
        {
            _context = context;
        }

        [Authorize]
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetList([FromRoute] Guid id)
        {
            try
            {
                var toDoList = _context.ToDoList.Where(list => list.UserId == id);
                
                return Ok(toDoList.ToList());
            }
            catch (Exception ex)
            {
                return BadRequest("Error getting tasks, please try again.");
            }
        }

        [Authorize]
        [HttpPost("[action]/{id}")]
        public async Task<IActionResult> AddTask([FromRoute] Guid id, [FromBody] ToDo task)
        {
            task.CreatedDate = DateTime.Now;
            task.LastUpdated = DateTime.Now;
            task.Id = Guid.NewGuid();

            try
            {
                _context.ToDoList.Add(task);
                _context.SaveChanges();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest("Error saving task, please try again.");
            }
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<IActionResult> EditTask([FromBody] ToDo task)
        {
            try
            {
                var oldTaskList = _context.ToDoList.Where(t => t.Id == task.Id && t.UserId == task.UserId);
                
                if (oldTaskList.Count() > 0)
                {
                    ToDo oldTask = oldTaskList.ToList().FirstOrDefault();

                    oldTask.Title = task.Title;
                    oldTask.Description = task.Description;
                    oldTask.DueDate = task.DueDate;
                    oldTask.LastUpdated = DateTime.Now;

                    _context.SaveChanges();

                    return Ok();
                }
                else
                {
                    return BadRequest("Invalid task, not found in database.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest("Error updating task, please try again.");
            }
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteTask([FromBody] ToDo task)
        {
            try
            {
                var taskList = _context.ToDoList.Where(t => t.Id == task.Id && t.UserId == task.UserId);

                if (taskList.Count() > 0)
                {
                    ToDo deletedTask = taskList.ToList().FirstOrDefault();

                    _context.ToDoList.Remove(deletedTask);

                    _context.SaveChanges();

                    return Ok();
                }
                else
                {
                    return BadRequest("Invalid task, not found in database.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest("Error deleting task, please try again.");
            }
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<IActionResult> ToggleTaskComplete([FromBody] ToDo task)
        {
            try
            {
                var taskList = _context.ToDoList.Where(t => t.Id == task.Id && t.UserId == task.UserId);

                if (taskList.Count() > 0)
                {
                    ToDo updateTask = taskList.ToList().FirstOrDefault();

                    updateTask.IsCompleted = task.IsCompleted;

                    _context.SaveChanges();

                    return Ok();
                }
                else
                {
                    return BadRequest("Invalid task, not found in database.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest("Error deleting task, please try again.");
            }
        }
    }
}