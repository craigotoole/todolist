import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Todo } from 'src/app/models/todo/todo';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from 'src/app/services/store/store.service';
import { ToDoListService } from 'src/app/services/to-do-list/to-do-list.service';
import { User } from 'src/app/models/user/user';
import { AppConfig } from 'src/app/config/app-config';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from "jwt-decode";
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html'
})
export class AddTaskComponent implements OnInit {

  userId: string;
  addTaskForm: FormGroup;
  addTaskData: Todo;

  constructor(private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly spinner: NgxSpinnerService,
    private readonly storeService: StoreService,
    private readonly listService: ToDoListService,
    private readonly toastrService: ToastrService,
    private readonly authService: AuthService) { }

  ngOnInit() {
    this.spinner.hide();

    this.userId = this.authService.decodeToken(this.storeService.get(AppConfig.TOKEN))['unique_name'];

    this.addTaskData = new Todo();
    this.addTaskForm = this.fb.group({
      title: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      dueDate: new FormControl(null, Validators.required)
    });
  }

  submitForm() {
    console.log(this.addTaskForm);

    this.spinner.show();

    this.addTaskData.title = this.addTaskForm.get('title').value;
    this.addTaskData.description = this.addTaskForm.get('description').value;
    this.addTaskData.dueDate = this.addTaskForm.get('dueDate').value;
    this.addTaskData.isCompleted = false;
    this.addTaskData.userId = this.userId;

    this.listService.addTask(this.userId, this.addTaskData).subscribe(
      result => {
        this.router.navigate(['/home']);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.toastrService.error("Add task error", error.error)
        console.log(error)
      });
  }
}
