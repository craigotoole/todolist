import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo/todo';
import { User } from 'src/app/models/user/user';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from 'src/app/services/store/store.service';
import { ToDoListService } from 'src/app/services/to-do-list/to-do-list.service';
import { AppConfig } from 'src/app/config/app-config';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  providers: [DatePipe]  
})
export class ViewTaskComponent implements OnInit {

  user: User;
  addTaskForm: FormGroup;
  task: Todo;

  hideEdit: boolean = false;
  editTask: boolean = false;

  constructor(private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly spinner: NgxSpinnerService,
    private readonly storeService: StoreService,
    private readonly listService: ToDoListService,
    private readonly datePipe: DatePipe,
    private readonly toastrService: ToastrService) { }

  ngOnInit() {
    this.spinner.hide();

    this.task = this.storeService.getObject(AppConfig.TASK);

    this.addTaskForm = this.fb.group({
      title: new FormControl(this.task.title, Validators.required),
      description: new FormControl(this.task.description, Validators.required),
      dueDate: new FormControl(this.datePipe.transform(this.task.dueDate, 'dd-MM-yyyy'), Validators.required),
      lastUpdated: new FormControl(this.datePipe.transform(this.task.lastUpdated, 'dd/MM/yyyy HH:mm:ss'), Validators.required)
    });

    this.disableForm();
  }

  submitForm() {
    this.spinner.show();

    this.task.title = this.addTaskForm.get('title').value;
    this.task.description = this.addTaskForm.get('description').value;
    this.task.dueDate = this.addTaskForm.get('dueDate').value;

    this.listService.editTask(this.task).subscribe(
      result => {
        this.router.navigate(['/home']);
        this.storeService.clearItem(AppConfig.TASK);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.toastrService.error("Edit error", error.error)
        console.log(error)
      });
  }

  disableForm() {
    this.addTaskForm.controls['title'].disable();
    this.addTaskForm.controls['title'].setValue(this.task.title);
    this.addTaskForm.controls['description'].disable();
    this.addTaskForm.controls['description'].setValue(this.task.description);
    this.addTaskForm.controls['dueDate'].disable();
    this.addTaskForm.controls['dueDate'].setValue(this.datePipe.transform(this.task.dueDate, 'dd-MM-yyyy'));
    this.addTaskForm.controls['lastUpdated'].disable();

    this.hideEdit = false;
    this.editTask = false;
  }

  enableForm() {
    this.addTaskForm.controls['title'].enable();
    this.addTaskForm.controls['description'].enable();
    this.addTaskForm.controls['dueDate'].enable();

    this.hideEdit = true;
    this.editTask = true;

  }
}
