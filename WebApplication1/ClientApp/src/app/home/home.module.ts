import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddTaskComponent } from './add-task/add-task.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ViewTaskComponent } from './view-task/view-task.component';


@NgModule({
  declarations: [HomeComponent, AddTaskComponent, ViewTaskComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FontAwesomeModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class HomeModule { }
