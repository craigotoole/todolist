import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToDoListService } from 'src/app/services/to-do-list/to-do-list.service';
import { StoreService } from 'src/app/services/store/store.service';
import { User } from 'src/app/models/user/user';
import { AppConfig } from 'src/app/config/app-config';
import { Todo } from 'src/app/models/todo/todo';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-regular-svg-icons';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as jwt_decode from "jwt-decode";
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  userId: string;
  toDoList: Todo[];

  completedTasks: Todo[];
  activeTasks: Todo[];

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;

  constructor(private readonly spinner: NgxSpinnerService,
    private readonly listService: ToDoListService,
    private readonly storeService: StoreService,
    private readonly router: Router,
    private readonly toastrService: ToastrService,
    private readonly authService: AuthService) {
    this.completedTasks = [];
    this.activeTasks = [];
  }

  ngOnInit() {
    this.userId = this.authService.decodeToken(this.storeService.get(AppConfig.TOKEN))['unique_name'];

    this.getTaskList(this.userId);
  }

  makeTaskLists() {
    this.completedTasks = [];
    this.activeTasks = [];
    for (let o of this.toDoList) {
      if (o.isCompleted) {
        this.completedTasks.push(o);
      } else {
        this.activeTasks.push(o);
      }
    }
  }

  addTask() {
    this.spinner.show();
    this.router.navigate(['/home/add-task']);
  }

  viewTask(task: Todo) {
    this.storeService.storeObject(AppConfig.TASK, task);
    this.router.navigate(['/home/view-task']);
  }

  getTaskList(id: string) {
    this.listService.getList(id).subscribe(
      result => {
        this.storeService.storeObject(AppConfig.TO_DO_LIST, result);
        this.toDoList = result;
        this.makeTaskLists();
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.toastrService.error("Loading task error", error.error)
        console.log(error)
      });
  }

  deleteTask(task: Todo) {
    this.spinner.show();
    this.listService.deleteTask(task).subscribe(
      result => {
        this.getTaskList(this.userId);
      },
      error => {
        this.spinner.hide();
        this.toastrService.error("Delete task error", error.error)
        console.log(error)
      });
  }

  reactiveTask(item: Todo) {
    this.spinner.show();
    item.isCompleted = false;

    this.listService.toggleTaskCompleted(item).subscribe(
      result => {
        this.getTaskList(this.userId);
      },
      error => {
        this.spinner.hide();
        this.toastrService.error("Reactivate task error", error.error)
        console.log(error)
      });
  }

  completeTask(item: Todo) {
    this.spinner.show();
    item.isCompleted = true;

    this.listService.toggleTaskCompleted(item).subscribe(
      result => {
        this.getTaskList(this.userId);
      },
      error => {
        this.spinner.hide();
        this.toastrService.error("Complete task error", error.error)
        console.log(error)
      });

  }
}
