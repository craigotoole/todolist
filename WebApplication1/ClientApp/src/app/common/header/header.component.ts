import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { StoreService } from 'src/app/services/store/store.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  showLogout: boolean = false;
  faSignOutAlt = faSignOutAlt;

  constructor(private readonly router: Router,
    private readonly storeService: StoreService,
    private readonly authService: AuthService) { }

  ngOnInit() {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url != "/") {
          this.showLogout = true;
        } else {
          this.showLogout = false;
        }
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
