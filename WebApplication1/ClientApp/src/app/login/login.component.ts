import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Login } from '../models/login/login';
import { Router } from '@angular/router';
import { LoginService } from '../services/login/login.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../services/store/store.service';
import { AppConfig } from '../config/app-config';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../services/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginData: Login;

  constructor(private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly loginService: LoginService,
    private readonly spinner: NgxSpinnerService,
    private readonly storeService: StoreService,
    private readonly toastrService: ToastrService,
    private readonly authService: AuthService) { }

  ngOnInit() {
    if (this.authService.isAuth()) {
      this.router.navigate(['/home']);
    }

    this.spinner.hide();
    this.loginData = new Login();
    this.loginForm = this.fb.group({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required), 
    });
  }

  submitForm() {
    this.spinner.show();
    this.loginData.username = this.loginForm.get('username').value;
    this.loginData.password = this.loginForm.get('password').value;

    this.loginService.login(this.loginData).subscribe(
      result => {
        this.storeService.store(AppConfig.TOKEN, result.userToken);
        this.router.navigate(['/home']);
      },
      error => {
        this.spinner.hide();
        this.toastrService.error("Login error", error.error)
        console.log(error)
      });
  }
}
