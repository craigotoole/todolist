export class Todo {
  id: string;
  userId: string;
  title: string;
  description: string;
  createdDate: Date;
  dueDate: Date;
  lastUpdated: Date;
  isCompleted: boolean;
}
