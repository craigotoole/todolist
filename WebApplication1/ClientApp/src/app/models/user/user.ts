import { Todo } from '../todo/todo';

export class User {
  id: string;
  username: string;
  password: string;
  toDoList: Todo[];
}
