import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastr: ToastrService) {
  }

  success(title: string, message: string) {
    this.toastr.success(title, message, {
      timeOut: 8000,
      positionClass: 'toast-bottom-center'
    });
  }

  warning(title: string, message: string) {
    this.toastr.warning(title, message, {
      timeOut: 8000,
      positionClass: 'toast-bottom-center'
    });
  }

  error(title: string, message: string) {
    this.toastr.error(title, message, {
      timeOut: 8000,
      positionClass: 'toast-bottom-center'
    });
  }
}
