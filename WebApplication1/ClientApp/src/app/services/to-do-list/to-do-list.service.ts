import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from 'src/app/config/app-config';
import { Todo } from 'src/app/models/todo/todo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToDoListService {

  constructor(private http: HttpClient) { }

  getList(id: string): Observable<Todo[]> {
    return this.http.get<Todo[]>(AppConfig.API_ENDPOINT + AppConfig.LIST + "/" + AppConfig.GET_LIST + "/" + id);
  }

  addTask(id: string, task: Todo): Observable<any> {
    return this.http.post(AppConfig.API_ENDPOINT + AppConfig.LIST + "/" + AppConfig.ADD_TASK + "/" + id, task);
  }

  editTask(task: Todo): Observable<any> {
    return this.http.post(AppConfig.API_ENDPOINT + AppConfig.LIST + "/" + AppConfig.EDIT_TASK, task);
  }

  deleteTask(task: Todo): Observable<any> {
    return this.http.post(AppConfig.API_ENDPOINT + AppConfig.LIST + "/" + AppConfig.DELETE_TASK, task);
  }

  toggleTaskCompleted(task: Todo): Observable<any> {
    return this.http.post(AppConfig.API_ENDPOINT + AppConfig.LIST + "/" + AppConfig.TOGGLE_COMPLETE, task);
  }
}
