import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor() { }

  store(key: string, value: any) {
    localStorage.setItem(key, value);
  }

  storeObject(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  get(key: string) {
    return localStorage.getItem(key);
  }

  getObject(key: string) {
    return JSON.parse(localStorage.getItem(key));
  }

  clearItem(key: string) {
    localStorage.removeItem(key);
  }

  clearAll() {
    localStorage.clear();
  }
}
