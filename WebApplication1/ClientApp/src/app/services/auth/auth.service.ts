import { Injectable } from '@angular/core';
import { StoreService } from '../store/store.service';
import { AppConfig } from 'src/app/config/app-config';
import { User } from 'src/app/models/user/user';
import { Router } from '@angular/router';
import * as jwt_decode from "jwt-decode";
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private readonly storeService: StoreService,
    private readonly router: Router,
    private readonly spinnerService: NgxSpinnerService,
    private readonly toastService: ToastrService) { }

  isAuth() {

    let token = this.decodeToken(this.storeService.get(AppConfig.TOKEN));
    
    if (token == null) {
      return false;
    } else if (token.exp === undefined) {
      return false;
    } else if (token.exp.valueOf() <= new Date().getTime().valueOf() / 1000) {
      return false;
    } else {
      return true;
    }
  }

  logout() {
    this.storeService.clearAll();
    this.spinnerService.hide();
    this.router.navigate(['']);
  }
  
  decodeToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }
}
