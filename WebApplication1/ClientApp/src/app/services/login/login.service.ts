import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from 'src/app/models/login/login';
import { Token } from 'src/app/models/token/token';
import { AppConfig } from 'src/app/config/app-config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(loginData: Login): Observable<Token> {
    return this.http.post<Token>(AppConfig.API_ENDPOINT + AppConfig.SECURITY + "/" + AppConfig.LOGIN, loginData);
  }
}
