import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class HomeGuardGuard implements CanActivate  {

  constructor(private readonly auth: AuthService,
    private readonly router: Router) { }

  canActivate(): boolean {
    if (!this.auth.isAuth()) {
      this.auth.logout();
      return false;
    }
    return true;
  }
}
