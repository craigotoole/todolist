import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from '../services/auth/auth.service';
import { Observable, throwError, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppConfig } from '../config/app-config';
import { StoreService } from '../services/store/store.service';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private readonly auth: AuthService,
    private readonly storeService: StoreService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = this.storeService.get(AppConfig.TOKEN);
    if (token != null) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401) {
        this.auth.logout();
      }
      throw err;
    }));
  }
}
