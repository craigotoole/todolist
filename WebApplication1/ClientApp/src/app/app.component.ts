import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'ClientApp';

  constructor(private readonly spinner: NgxSpinnerService) {
    this.spinner.show();
  }

  ngOnInit() {
  }
}
