export class AppConfig {
  // API CONSTANTS

  public static API_ENDPOINT = '/api/';
  public static SECURITY = 'Security';
  public static LOGIN = 'Login';
  public static LIST = 'List';
  public static GET_LIST = 'GetList';
  public static ADD_TASK = 'AddTask';
  public static EDIT_TASK = 'EditTask';
  public static DELETE_TASK = 'DeleteTask';
  public static TOGGLE_COMPLETE = 'ToggleTaskComplete';

  //LOCAL STORAGE CONSTANTS
  public static USER = 'user';
  public static TOKEN = 'token';
  public static TO_DO_LIST = 'toDoList';
  public static TASK = 'task';
}
