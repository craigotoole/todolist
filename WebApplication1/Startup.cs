using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IO;
using System.Text;
using ToDoList.Models.API;
using ToDoList.Models.DatabaseConnection;
using WebApplication1.Models.AppSettings;

namespace WebApplication1
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ToDoListDbContext>(opt =>
                opt.UseInMemoryDatabase("ToDoList"));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.SecretKey);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist/ClientApp";
            });

            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ToDoListDbContext _dbContext)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ToDoListDbContext>();

                AddTestData(context);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.Use(async (context, next) =>
                {
                    await next();
                    if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                    {
                        context.Request.Path = "/index.html";
                        await next();
                    }
                });
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseAuthentication();

            app.UseCors("CorsPolicy");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
        private static void AddTestData(ToDoListDbContext context)
        {
            context.Users.AddRange(
                    new User
                    {
                        Id = new Guid("4b4eee41-f82a-4232-9fc8-949823453211"),
                        Username = "CraigOT",
                        Password = "l37LKsgqxeU1vg+1U9aqOFdAFYmxROxL54npmP4PCOywdrk3lRD0of0geXiYvqNo",
                    },
                    new User
                    {
                        Id = new Guid("0ee36e80-e446-4ad8-9f0b-3da134de5902"),
                        Username = "JohnDoe",
                        Password = "tkF7DXHIssLGGegTsTJMJn7Q52SWC7IovAFrRyBzUsh6lh+DM1ZsSGS6uXshLDn5",
                    },
                    new User
                    {
                        Id = new Guid("475457c5-73a3-427a-a35a-90f7ba66c8e2"),
                        Username = "MarkLong",
                        Password = "oNZfLi32+x8iig7/zK5r25jC1fEx8XPFXgwM3p0hbQiola//I6Tc2G7HXlN7iNEM",
                    }
                );

            context.ToDoList.AddRange(
                new ToDo
                {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("4b4eee41-f82a-4232-9fc8-949823453211"),
                    Title = "Make Dinner.",
                    Description = "Go to the kitchen and make dinner for the family.",
                    CreatedDate = DateTime.Now.AddDays(-10),
                    DueDate = DateTime.Now.AddDays(10),
                    LastUpdated = DateTime.Now.AddDays(-5),
                    IsCompleted = false
                },
                new ToDo
                {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("4b4eee41-f82a-4232-9fc8-949823453211"),
                    Title = "Walk Dog.",
                    Description = "Bring the dog for a walk to the beach.",
                    CreatedDate = DateTime.Now.AddDays(-1),
                    DueDate = DateTime.Now.AddDays(10),
                    LastUpdated = DateTime.Now.AddDays(-1),
                    IsCompleted = true
                },
                new ToDo
                {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("0ee36e80-e446-4ad8-9f0b-3da134de5902"),
                    Title = "Submit Assignment.",
                    Description = "Submit my Chemistry assignment.",
                    CreatedDate = DateTime.Now.AddDays(-7),
                    DueDate = DateTime.Now.AddDays(5),
                    LastUpdated = DateTime.Now,
                    IsCompleted = false
                },
                new ToDo
                {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("475457c5-73a3-427a-a35a-90f7ba66c8e2"),
                    Title = "Submit Assignment.",
                    Description = "Submit my Chemistry assignment.",
                    CreatedDate = DateTime.Now.AddDays(-2),
                    DueDate = DateTime.Now.AddDays(2),
                    LastUpdated = DateTime.Now.AddDays(-1),
                    IsCompleted = false
                }

                );
            context.SaveChanges();
        }
    }
}
